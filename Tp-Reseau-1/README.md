# B2 Réseau 2022 - TP1

# TP1 - Mise en jambes

**Dans ce TP, on va explorer le réseau de vos clients, vos PC.**

Le terme *réseau* désigne au sens large toutes les fonctionnalités d'un PC permettant de se connecter à d'autres machines.  

De façon simplifiée, on appelle *stack TCP/IP* ou *pile TCP/IP* l'ensemble de logiciels qui permettent d'utiliser et configurer des [cartes réseau](../../cours/lexique.md#carte-r%C3%A9seau-ou-interface-r%C3%A9seau) avec des adresses IP.

C'est juste un gros mot savant pour désigner tout ce qui touche de près ou de loin au réseau dans une machine okay ? :)

Lorsque l'on parle de réseau, on désigne souvent par *client* tout équipement qui porte une adresse IP.

Donc vos PCs sont des *clients*, et on va explorer leur *réseau*, c'est à dire, leur *pile TCP/IP*.

# Sommaire
- [B2 Réseau 2022 - TP1](#b2-réseau-2022---tp1)
- [TP1 - Mise en jambes](#tp1---mise-en-jambes)
- [Sommaire](#sommaire)
- [Déroulement et rendu du TP](#déroulement-et-rendu-du-tp)
- [I. Exploration locale en solo](#i-exploration-locale-en-solo)
  - [1. Affichage d'informations sur la pile TCP/IP locale](#1-affichage-dinformations-sur-la-pile-tcpip-locale)
    - [En ligne de commande](#en-ligne-de-commande)
    - [En graphique (GUI : Graphical User Interface)](#en-graphique-gui--graphical-user-interface)
    - [Questions](#questions)
  - [2. Modifications des informations](#2-modifications-des-informations)
    - [A. Modification d'adresse IP (part 1)](#a-modification-dadresse-ip-part-1)
    - [B. Table ARP](#b-table-arp)
    - [C. `nmap`](#c-nmap)
    - [D. Modification d'adresse IP (part 2)](#d-modification-dadresse-ip-part-2)
- [II. Exploration locale en duo](#ii-exploration-locale-en-duo)
  - [1. Prérequis](#1-prérequis)
  - [2. Câblage](#2-câblage)
  - [Création du réseau (oupa)](#création-du-réseau-oupa)
  - [3. Modification d'adresse IP](#3-modification-dadresse-ip)
  - [4. Utilisation d'un des deux comme gateway](#4-utilisation-dun-des-deux-comme-gateway)
  - [5. Petit chat privé](#5-petit-chat-privé)
  - [6. Firewall](#6-firewall)
- [III. Manipulations d'autres outils/protocoles côté client](#iii-manipulations-dautres-outilsprotocoles-côté-client)
  - [1. DHCP](#1-dhcp)
  - [2. DNS](#2-dns)
- [IV. Wireshark](#iv-wireshark)
- [Bilan](#bilan)

# Déroulement et rendu du TP

- Groupe de 2 jusqu'à 4 personnes. Il faut au moins deux PCs avec une prise RJ45 (Ethernet) par groupe
- Un câble RJ45 (fourni) pour connecter les deux PCs
- **Un compte-rendu par personne**
  - vu que vous travaillez en groupe, aucun problème pour copier/coller les parties à faire à plusieurs (tout le [`II.`](#ii-exploration-locale-en-duo))
  - une bonne partie est à faire de façon individuelle malgré tout (tout le [`I.`](#i-exploration-locale-en-solo) et le [`III.`](#iii-manipulations-dautres-outilsprotocoles-côté-client))
  - on prendra le temps, mais le travail devra être rendu à travers Github ou tout autre plateforme supportant le `md` (markdown)
- Le rendu doit :
  - comporter des réponses aux questions explicites
  - comporter la marche à suivre pour réaliser les étapes demandées :
    - en ligne de commande, copier/coller des commandes et leurs résultat
    - en interface graphique, screenshots ou nom des menus où cliquer (sinon ça peut vite faire 1000 screenshots)
  - par exemple, pour la partie `1.A.` je veux le la commande tapée et le résultat
    - un copier/coller ça me va très bien, mieux qu'un screen pour de la ligne de commande
  - de façon générale, tout ce que vous faites et qui fait partie du TP, vous me le mettez :)

**⚠️ ⚠️ Désactivez votre firewall pour ce TP. ⚠️ ⚠️**

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

En utilisant la ligne de commande (CLI) de votre OS :

**🌞 Affichez les infos des cartes réseau de votre PC**

``````
PS C:\Users\Dreasy>ipconfig /all
Wireless LAN adapter Wi-Fi:
    [...]
    Physical Address. . . . . . . . . : 8C-C6-81-78-EA-6A
    IPv4 Address. . . . . . . . . . . : 10.33.19.193
    [...]
``````
**🌞 Affichez votre gateway**

``````
PS C:\Users\Dreasy>ipconfig /all
Wireless LAN adapter Wi-Fi:
    [...]
    Default Gateway . . . . . . . . . : 10.33.19.254
    [...] 
``````
  
### En graphique (GUI : Graphical User Interface)

En utilisant l'interface graphique de votre OS :  

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

```
in control panel search 
Control Panel\Network and Internet\Network and Sharing Center
click on wifi link 
Details
```
[screenshots](../images/panel.png)

### Questions

- 🌞 à quoi sert la [gateway](../../cours/lexique.md#passerelle-ou-gateway) dans le réseau d'YNOV ?

    **La gateway d'ynov sert a communiqué avec le réseau exterieur**


## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

- changez l'adresse IP de votre carte WiFi pour une autre
- ne changez que le dernier octet
  - par exemple pour `10.33.1.10`, ne changez que le `10`
  - valeur entre 1 et 254 compris
  
  ```
  IPv4 Address. . . . . . . . . . . : 10.33.19.195(Duplicate)
  ```

🌞 **Il est possible que vous perdiez l'accès internet.** Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

*Ici j'ai plus d'accés car j'ai recuperé une ip qui etait deja attribué dans le reseau par le server dhcp*
   

---

- **NOTE :** si vous utilisez la même IP que quelqu'un d'autre, il se passerait la même chose qu'en vrai avec des adresses postales :
  - deux personnes habitent au même numéro dans la même rue, mais dans deux maisons différentes
  - quand une de ces personnes envoie un message, aucun problème, l'adresse du destinataire est unique, la lettre sera reçue
  - par contre, pour envoyer un message à l'une de ces deux personnes, le facteur sera dans l'impossibilité de savoir dans quelle boîte aux lettres il doit poser le message
  - ça marche à l'aller, mais pas au retour


# II. Exploration locale en duo

Owkay. Vous savez à ce stade :

- afficher les informations IP de votre machine
- modifier les informations IP de votre machine
- c'est un premier pas vers la maîtrise de votre outil de travail

On va maintenant répéter un peu ces opérations, mais en créant un réseau local de toutes pièces : entre deux PCs connectés avec un câble RJ45.

## 1. Prérequis

- deux PCs avec ports RJ45
- un câble RJ45
- **firewalls désactivés** sur les deux PCs

## 2. Câblage

Ok c'est la partie tendue. Prenez un câble. Branchez-le des deux côtés. **Bap.**

## Création du réseau (oupa)

Cette étape peut paraître cruciale. En réalité, elle n'existe pas à proprement parlé. On ne peut pas "créer" un réseau. Si une machine possède une carte réseau, et si cette carte réseau porte une adresse IP, alors cette adresse IP se trouve dans un réseau (l'adresse de réseau). Ainsi, le réseau existe. De fait.  

**Donc il suffit juste de définir une adresse IP sur une carte réseau pour que le réseau existe ! Bam.**



### 3. Modification d'adresse IP

<div align="center">
<img src="../images/change192168.png" alt="Changement adresse IP en graphique PC1">
</div>

<div align="center">
<img src="../images/bischange192168.png" alt="Changement adresse IP en graphique PC2">
</div>

```bash
# vérification des changements sur le premier ordinateur
C:\Users\Matteo>ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::e1db:4bc1:6c23:6d5f%10
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.137.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.137.1
```

```bash
# vérification des changements sur le deuxième ordinateur
Ethernet adapter Ethernet:

   Connection-specific DNS Suffix  . :
   Link-local IPv6 Address . . . . . : fe80::dd19:ecee:b04e:a3e7%22
   IPv4 Address. . . . . . . . . . . : 192.168.137.1
   Subnet Mask . . . . . . . . . . . : 255.255.255.252
```   

```bash
# ping du PC1 vers le PC2
PS C:\Users\Dreasy> ping 192.168.137.2

Pinging 192.168.137.2 with 32 bytes of data:
Reply from 192.168.137.2: bytes=32 time<1ms TTL=128
Reply from 192.168.137.2: bytes=32 time=2ms TTL=128
Reply from 192.168.137.2: bytes=32 time=1ms TTL=128
Reply from 192.168.137.2: bytes=32 time=1ms TTL=128

Ping statistics for 192.168.137.2:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 0ms, Maximum = 2ms, Average = 1ms
```

```bash
# ping du PC2 vers le PC1
C:\Users\Matteo>ping 192.168.137.1

Envoi d’une requête 'Ping'  192.168.137.1 avec 32 octets de données :
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.137.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```

```bash
# affichage et consultation de la table ARP du premier ordinateur
Interface: 192.168.137.1 --- 0x16
  Internet Address      Physical Address      Type
  192.168.137.2           04-d4-c4-e6-15-34     dynamic
  192.168.137.3           ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
```

```bash
# affichage et consultation de la table ARP du second ordinateur
C:\Users\Matteo>arp -a

[...]

Interface : 192.168.137.2 --- 0xa
  Adresse Internet      Adresse physique      Type
  192.168.137.1           a0-ce-c8-38-0c-17     dynamique
  192.168.137.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

## 4. Utilisation d'un des deux comme gateway

```bash
#requête vers un serveur connu
C:\Users\Matteo>ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=24 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=21 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=54
Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=54

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 21ms, Maximum = 24ms, Moyenne = 22ms
```

```bash
# vérification du passage des paquets par la passerelle
C:\Users\Matteo>tracert 1.1.1.1

Détermination de l’itinéraire vers one.one.one.one [1.1.1.1]
avec un maximum de 30 sauts :

  1    <1 ms     *        2 ms  DREASY [192.168.137.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     5 ms     5 ms     5 ms  10.33.19.254
  4     6 ms     6 ms     6 ms  137.149.196.77.rev.sfr.net [77.196.149.137]
  5    14 ms    12 ms    12 ms  108.97.30.212.rev.sfr.net [212.30.97.108]
```

## 5. Petit chat privé

```bash
# informations visibles depuis le pc serveur

PS C:\Users\Dreasy> ncat -l -p 8888
Coucou
Miew
g fAIm
```

```bash
# informations visibles depuis le pc client

PS C:\Users\Matteo> ncat  192.168.137.1 8888
libnsock ssl_init_helper(): OpenSSL legacy provider failed to load.

Coucou
Miew
g fAIm
```
## 6 - Firewall

```bash
# changement des deux pc entre temps puisqu'on ne pouvait pas installer netcat sur le pc qu'on nous avait prêté
```

```bash
# ipv4 indique le type de règle, -p icmp inidque le port ou protocole et les deux ip permetttent d'autoriser la source et la destination pour n'importe qui

$ sudo firewall-cmd --permanent --direct --add-rule ipv4 filter INPUT 0 -p icmp -s 0.0.0.0/0 -d 0.0.0.0/0 -j ACCEPT
$ sudo systemctl restart firewalld.service
```

```bash
# permet d'autoriser le port netcat

$ sudo firewall-cmd --add-port=8888/tcp --permanent && sudo firewall-cmd --add-port=8888/udp --permanent && sudo systemctl restart firwalld
```

# III. Manipulations d'autres outils/protocoles côté client

## 1 - DHCP

```bash
# affichage de l'adresse IP du DHCP

Carte réseau sans fil Wi-Fi :

[...]
   Passerelle par défaut. . . . . . . . . : 10.33.19.254
```

```bash
# Le bail DHCP expire le jeudi 29 septembre 2022 à 9h16. Il a une durée de 24 heures qui peut être modifié par un administrateur.
C:\Users\33785>ipconfig /all

Configuration IP de Windows

[...]
   Bail obtenu. . . . . . . . . . . . . . : mercredi 28 septembre 2022 09:16:42
   Bail expirant. . . . . . . . . . . . . : jeudi 29 septembre 2022 09:16:42
[...]
```

## 2 - DNS

```bash
# adresse du serveur DNS que connait mon ordinateur
Carte réseau sans fil Wi-Fi :

[...]
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
                                       1.1.1.1
```

```bash
# affichage de l'adresse IP de google.com, on fait la requête au serveur DNS de google, l'adresse IP de google.com est 142.250.75.228. 

C:\Users\33785>nslookup www.google.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    www.google.com
Addresses:  2a00:1450:4007:80d::2004
          142.250.75.228
```

```bash
# affichage de l'adresse IP d'ynov.com, on fait la requête au serveur DNS de google, l'adresse IP d'ynov.com est 104.26.10.233.

C:\Users\33785>nslookup www.ynov.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    www.ynov.com
Addresses:  2606:4700:20::ac43:4ae2
          2606:4700:20::681a:be9
          2606:4700:20::681a:ae9
          172.67.74.226
          104.26.11.233
          104.26.10.233
```

```bash
L'adresse IP du serveur auprès duquel on fait des requêtes est celle du serveur DNS de google, 8.8.8.8
```

```bash
# le nom de domaine associé à l'adresse IP est host-78-74-21-21.homerun.telia.com
C:\Users\33785>nslookup 78.74.21.21
Serveur :   dns.google
Address:  8.8.8.8

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21
```

```bash
# il n'y a pas de nom de domaine associé à cette adresse IP

C:\Users\33785>nslookup 92.146.54.88
Serveur :   dns.google
Address:  8.8.8.8

*** dns.google ne parvient pas à trouver 92.146.54.88 : Non-existent domain
```

# IV - Wireshark

```bash
# changement d'adresse IP parce que je ne suis pas à Ynov (c'est plus pour que je m'en souvienne qu'autre chose)

C:\Users\33785>ipconfig

Configuration IP de Windows

[...]

Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6. . . . . . . . . . . . . .: 2a04:cec0:1139:cd55:7938:13dc:dac0:406
   Adresse IPv6 temporaire . . . . . . . .: 2a04:cec0:1139:cd55:c9b0:a197:7de2:6976
   Adresse IPv6 de liaison locale. . . . .: fe80::7938:13dc:dac0:406%20
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.43.114
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : fe80::9fc4:fa7a:e4:6fe1%20
                                       192.168.43.1
```

```bash
# ping vers la passerelle

C:\Users\dreasy>ping 192.168.43.1

Envoi d’une requête 'Ping'  192.168.43.1 avec 32 octets de données :
Réponse de 192.168.43.1 : octets=32 temps=2 ms TTL=64
Réponse de 192.168.43.1 : octets=32 temps=4 ms TTL=64
Réponse de 192.168.43.1 : octets=32 temps=3 ms TTL=64
Réponse de 192.168.43.1 : octets=32 temps=8 ms TTL=64

Statistiques Ping pour 192.168.43.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 8ms, Moyenne = 4ms
```

<div align="center">
<img src="../images/ping.png" alt="Ping client vers passerelle">
</div>


  - un `netcat` entre vous et votre mate, branché en RJ45


Apres plusieurs essais l'installation du 'bon' netcat n'a pas pu aboutir suite a des interdictions de windows malgrés tout les antivirus/firewall desactiver 
Suite a cela cette partie n'a pas pu etre terminer.


# Bilan

**Vu pendant le TP :**

- visualisation de vos interfaces réseau (en GUI et en CLI)
- extraction des informations IP
  - adresse IP et masque
  - calcul autour de IP : adresse de réseau, etc.
- connaissances autour de/aperçu de :
  - un outil de diagnostic simple : `ping`
  - un outil de scan réseau : `nmap`
  - un outil qui permet d'établir des connexions "simples" (on y reviendra) : `netcat`
  - un outil pour faire des requêtes DNS : `nslookup` ou `dig`
  - un outil d'analyse de trafic : `wireshark`
- manipulation simple de vos firewalls

**Conclusion :**

- Pour permettre à un ordinateur d'être connecté en réseau, il lui faut **une liaison physique** (par câble ou par *WiFi*).  
- Pour réceptionner ce lien physique, l'ordinateur a besoin d'**une carte réseau**. La carte réseau porte une [adresse MAC](../../cours/lexique/README.md#mac-media-access-control).  
- **Pour être membre d'un réseau particulier, une carte réseau peut porter une adresse IP.**
Si deux ordinateurs reliés physiquement possèdent une adresse IP dans le même réseau, alors ils peuvent communiquer.  
- **Un ordintateur qui possède plusieurs cartes réseau** peut réceptionner du trafic sur l'une d'entre elles, et le balancer sur l'autre, servant ainsi de "pivot". Cet ordinateur **est appelé routeur**.
- Il existe dans la plupart des réseaux, certains équipements ayant un rôle particulier :
  - un équipement appelé **[*passerelle*](../../cours/lexique/README.md#passerelle-ou-gateway)**. C'est un routeur, et il nous permet de sortir du réseau actuel, pour en joindre un autre, comme Internet par exemple
  - un équipement qui agit comme **serveur DNS** : il nous permet de connaître les IP derrière des noms de domaine
  - un équipement qui agit comme **serveur DHCP** : il donne automatiquement des IP aux clients qui rejoigne le réseau