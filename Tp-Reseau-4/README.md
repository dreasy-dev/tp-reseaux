# TP 4 : TCP, UDP et services réseau

- [I. First steps](#i-first-steps)
- [II. Mise en place](#ii-mise-en-place)
    - [1. SSH](#1-ssh)
    - [2. NFS](#2-nfs)
    - [1. DNS](#3-dns)

## I. First steps

**🌞 Déterminez, pour 5 applications, si c'est du TCP ou de l'UDP**

J'ai choisi 5 applications pour faire le TP :

- `Site : Spotify` qui utilise du `TCP`
    - IP : `104.199.65.124`
    - Port source : 3311
    - Port destinataire : 4070

- `Site : Discord` qui utilise du `TCP`
    - IP : `162.159.133.234`
    - Port source : 11930
    - Port destinataire : 443

- `Site : Telegram` qui utilise du `TCP`
    - IP : `95.100.95.167`
    - Port source : 1110
    - Port destinataire : 443

- `Site : telemat.org` qui utilise du `TCP`
    - IP : `217.69.21.67`
    - Port source : 3681
    - Port destinataire : 443

- `Site : Star Stable` qui utilise du `TCP`
    - IP : `18.66.2.115`
    - Port source : 8540
    - Port destinataire : 80

**🌞 Demandez l'avis à votre OS**

- `Site : Spotify` qui utilise du `TCP`
    - 🦈 [Capture Wireshark](./sources/wireshark-app1.pcapng)
    ```
    C:\Users\33785>netstat -an | findstr 4070
        TCP    192.168.1.20:3311      104.199.65.124:4070    ESTABLISHED
    ```
- `Site : Discord` qui utilise du `TCP`
    - 🦈 [Capture Wireshark](./sources/wireshark-app2.pcapng)
    ```
    C:\Users\33785>netstat -an | findstr 162.159
        TCP    192.168.1.20:1024      162.159.136.234:https  ESTABLISHED
    ```
- `Site : Telegram` qui utilise du `TCP`
    - 🦈 [Capture Wireshark](./sources/wireshark-app3.pcapng)
    ```
    C:\Users\33785>netstat -an | findstr 1110
        TCP    192.168.1.20:1110      95.100.95.167:443      ESTABLISHED
    ```
- `Site : Telemat` qui utilise du `TCP`
    - 🦈 [Capture Wireshark](./sources/wireshark-app4.pcapng)
    ```
    C:\Users\33785>netstat -an | findstr 217.69.21.67
        TCP    192.168.1.20:3681      217.69.21.67:443       CLOSE_WAIT
        TCP    192.168.1.20:3684      217.69.21.67:443       CLOSE_WAIT

    # Les sessions HTTP sont terminées, les données sont envoyées, c'est pour ça que l'état est en CLOSE_WAIT
    ```
- `Site : Spotify` qui utilise du `TCP`
    - 🦈 [Capture Wireshark](./sources/wireshark-app5.pcapng)
    ```
    C:\Users\33785>netstat -an | findstr 18.66.2.115
        TCP    192.168.1.20:8540      18.66.2.115:80         ESTABLISHED
    ```

## II. Mise en place

### 1. SSH

**🌞 Examinez le trafic dans Wireshark**

`SSH` utilise un protocole `TCP`.

SSH signifie Secure Socket Shell, il faut donc être sûr que tout est sécurisé, et assurer la bonne réception des paquets, dans l'odre, etc.

L'avantage en vitesse de l'UDP n'est pas nécessaire pour du SSH.

Malgré plusieurs essais, impossible d'obtenir un FINACK, la session se termine juste par un reset

🦈 [Capture Wireshark](./sources/wireshark-ssh.pcapng)

**🌞 Demandez aux OS**

- Depuis ma machine :
    ```
    C:\Users\33785>netstat -an | findstr 22
        TCP    10.3.1.11:1633         10.3.1.13:22           ESTABLISHED
    ```
- Depuis la VM :
    ```
    [user@localhost ~]$ netstat -an | grep 22
        [...]
        tcp        0     52 10.3.1.13:22            10.3.1.11:1633          ESTABLISHED
    ```

### 2. NFS

**🌞 Mettez en place un petit serveur NFS sur l'une des deux VMs**

**🌞 Wireshark it !**

🦈 [Capture Wireshark](./sources/wireshark-nfs.pcapng)


